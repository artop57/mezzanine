grid_breakpoint = 1024;

window.onload = function() {

    $('html').trigger('DOMLoaded');
}

$(document).ready(function() {


    $(window).on('resize', function() {
        setTimeout(function() {
            setUpMenu();
        }, 1000);
    });
    setUpMenu();

    toTopScrolling = true;
    scrolling = false;

    $('#back-to-top').click(function() {

        $(this).fadeOut('fast');
        if (typeof $.fn.fullpage.moveTo === 'function') {
            $.fn.fullpage.moveTo(1);
            toTopScrolling = true;
            $('#back-to-top').fadeOut();
        }
        else {
            $('html, body').animate({
                scrollTop: 0
            }, 600, function() {
                toTopScrolling = true;
                scrolling = false;
            });
        }
    });
    $(window).on('scroll', function() {
        scrolling = true;
        if (toTopScrolling) {
            $('#back-to-top').fadeIn();
            toTopScrolling = false;
        }
        else if (window.scrollY <= 50 && !scrolling) {
            $('#back-to-top').fadeOut();
            toTopScrolling = true;
        }
    });

    $(document).on('keypress', function(e) {
        if (e.keyCode == 37 || e.keyCode == 38 || e.keyCode == 39 || e.keyCode == 40) {
            e.preventDefault();
            return false;
        }
    });

});

function setUpMenu() {
    w = 0;
    $('.nav li').each(function() {
        w += $(this).outerWidth();
    });
    $('.nav').width(w);
}

function setupAccordeon() {
    if (window.innerWidth < 1650) {
        $('.accordeon li').click(function() {
            var show = false;
            if ($(this).children('.content').css('display') === 'none') {
                show = true;
            }

            $(this).parent('.accordeon').children('li').children('.content').slideUp('fast');
            $(this).parent('.accordeon').children('li').children('.link').children('img').removeClass('rotate');

            if (show) {

                obj = $(this);
                obj.children('.content').slideDown();
                obj.children('.link').children('img').addClass('rotate');
            }
        });
    }
    else {
        $('.accordeon li .content').show();
        $('.accordeon li img').addClass('rotate');
    }
}