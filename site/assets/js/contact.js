$(document).ready(init_contact);

function init_contact() {

    $('#form-contact').ajaxForm({
        type: "POST",
        dataType: "json",
        beforeSubmit: function() {
            $('.response').hide();
            $('.response').removeClass('error');
            $('.response').removeClass('valid');
            $('.loader').show();
        },
        success: function(data, jqForm, options) {

            $('.loader').hide();

            if (data.error) {
                showError(data);
            }
            else {
                $('.response').addClass('valid');
                $('.response').html($('.response').attr('data-sent'));
                $('.response').fadeIn('fast');
            }
        },
        error: function(data) {
            $('.loader').hide();
            showError();
        }
    });
}

function showError(data) {
    $('.response').addClass('error');
    if(typeof data.errorType !== 'undefined') {
        $('.response').html($('.response').attr('data-error-' + data.errorType));
    }
    else {
        $('.response').html($('.response').attr('data-error'));
    }
    
    $('.response').fadeIn('fast');
}