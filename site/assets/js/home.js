$(document).ready(init_home);

function init_home() {
    
    $(window).on('resize', function() {
        setupOverlays()
    });
    setupOverlays();
    
    $('.aside').on('mouseenter', function() {
        $(this).children('.overlay').fadeIn('fast');   
    });
    $('.aside').on('mouseleave', function() {
        if($(this).children('.overlay').css('display') !== 'none') {
            $(this).children('.overlay').fadeOut('fast');
        }
    });
    $('.aside').on('click', function() {
        if($(this).children('.overlay').css('display') === 'none') {
            $(this).children('.overlay').fadeIn('fast'); 
        }
        else {
            $(this).children('.overlay').fadeOut('fast');
        }
    });
}

function setupOverlays() {
    $('.aside .overlay').each(function() {
        $(this).height( $(this).parent('.aside').height() - 40 );
        $(this).width( $(this).parent('.aside').width() - 40 );
    });
}