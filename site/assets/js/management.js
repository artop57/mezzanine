$(document).ready(init_conseil_et_management);


function init_conseil_et_management() {

    if (window.innerWidth >= grid_breakpoint) {

        init_fullpage();
        setupAccordeon();
    }
}

function init_fullpage() {
    $.fn.fullpage({
        verticalCentered: false,
        resize: false,
        scrollingSpeed: 700,
        easing: 'easeOutQuart',
        menu: false,
        navigation: true,
        navigationPosition: 'right',
        slidesNavigation: true,
        slidesNavPosition: 'bottom',
        loopBottom: false,
        loopTop: false,
        loopHorizontal: true,
        autoScrolling: true,
        scrollOverflow: false,
        css3: true,
        paddingTop: '0',
        paddingBottom: '0',
        fixedElements: '#back-to-top',
        keyboardScrolling: true,
        touchSensitivity: 15,
        continuousVertical: false,
        animateAnchor: true,
        //events
        onLeave: function(index, direction) {

            if (index === 2 && direction === 'up') {
                $('#back-to-top').fadeOut();
            }
            else {
                $('#back-to-top').fadeIn();
            }
        },
//            afterLoad: function(anchorLink, index) {
//            },
//            afterRender: function() {
//            },
//            afterSlideLoad: function(anchorLink, index, slideAnchor, slideIndex) {
//            },
//            onSlideLeave: function(anchorLink, index, slideIndex, direction) {
//            }
    });
}