<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Page extends My_Controller {

    public function index() {
        redirect('accueil');
    }

    public function notFound() {
        header("HTTP/1.0 404 Not Found");
        $this->render('page/notFound', array(
            'page_alias' => 'notFound',
        ));
    }

    public function sendMail() {

        $data = array();

        $name = $this->input->post("name");
        $email = $this->input->post("email");
        $phone = $this->input->post("phone");
        if(!$phone) $phone = 'Non renseigné';
        $message = $this->input->post("message");

        // VALIDATION /////////////////////////////////////////////////////////////////

        $this->load->library('form_validation');
        
        $this->form_validation->set_rules('name', 'Name', 'trim|required|max_length[100]');
        $this->form_validation->set_rules('phone', 'Phone', 'trim|max_length[50]');
        $this->form_validation->set_rules('email', 'Email', 'trim|valid_email|required|max_length[200]');
        $this->form_validation->set_rules('message', 'Message', 'trim|required|max_length[2500]');

        if ($this->form_validation->run() == FALSE) {
            
            $data['error'] = true;
            $data['errorType'] = 'validation';
            exit(json_encode($data));
        }

        // EMAIL /////////////////////////////////////////////////////////////////

        $this->load->library('email');

        $config['mailtype'] = 'html';
        $this->email->initialize($config);

        $this->email->from($email, $name);
        $this->email->to($this->config->item('email_contact'));

        $this->email->subject('Contact Mezzanine');
        
        $message .= '<br><br><p>' . $name . '</p><p>Tel: ' . $phone .'</p>';
        
        $this->email->message(nl2br($message));

        if ($this->email->send()) {
            $data['error'] = false;
            exit(json_encode($data));
        } else {
            $data['error'] = true;
            exit(json_encode($data));
        }
    }

    public function home() {


        $imgs = array('tour2.png');
        $img = $imgs[rand(0, count($imgs) - 1)];

        $this->loadLang('home');

        $this->render('page/home', array(
            'page_alias' => 'home',
            'img' => $img,
        ));
    }

    public function investment() {

        $this->loadLang('investment');

        $this->render('page/investment', array(
            'page_alias' => 'investment',
        ));
    }

    public function management() {

        $this->loadLang('management');

        $this->render('page/management', array(
            'page_alias' => 'management',
        ));
    }

    public function about() {

        $this->loadLang('about');

        $this->render('page/about', array(
            'page_alias' => 'about',
        ));
    }

    public function contact() {

        $this->loadLang('contact');

        $this->render('page/contact', array(
            'page_alias' => 'contact',
        ));
    }

}
