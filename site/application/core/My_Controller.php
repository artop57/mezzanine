<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

abstract class My_Controller extends CI_Controller {

    protected $data;
    private $twig_instance;
    protected $uri_string;

    public function __construct() {
        parent::__construct();
        $this->load->library('twig');
        $this->load->helper('url');

        $this->twig_instance = $this->twig->getTwig();
        $this->uri_string = uri_string();
        $this->data = array();

        // TWIG FUNCTIONS

        $this->registerTwigFunctions();
        $this->registerTwigFilters();

        // LOAD LANG
        $this->loadLang('main');

        if (array_key_exists($this->uri_string, $this->router->routes) || array_key_exists(str_replace('/', '', $this->uri_string), $this->router->routes)) {
            header("HTTP/1.1 200 OK");
        }
    }

    /**
     * Render current page template
     */
    protected function render($template = null, $data = array()) {

        if ($template == null) {
            $template = $this->getCurrentTemplateName();
        }

        echo($this->loadTemplate($template, $data));
    }

    protected function loadTemplate($template, $data = array()) {
        try {
            $template = $this->twig_instance->loadTemplate($template . '.html.twig');
        } catch (Exception $ex) {
            redirect('notFound');
        }


        $this->data = array_merge($this->data, $data);
        

        $uri = str_replace('/', '', $this->uri_string);


        if ($this->config->item('language') == 'english') {
            
            $this->data['lang'] = 'en';

            $uri_to_fr = array(
                'home' => 'accueil',
                'investment' => 'investissement',
                'advisory' => 'conseil',
                'about' => 'a-propos',
                'contact' => 'contact',
            );

            $this->data['uri_en'] = 'en/' . $uri;
            
            if(array_key_exists($uri, $uri_to_fr)) {
                $this->data['uri_fr'] = 'fr/' . $uri_to_fr[$uri];
            }
            else {
                $this->data['uri_en'] = 'en/' . $uri;
            }
        } 
        elseif ($this->config->item('language') == 'francais') {
            
            $this->data['lang'] = 'fr';
            
            $uri_to_en = array(
                'accueil' => 'home',
                'investissement' => 'investment',
                'conseil' => 'advisory',
                'a-propos' => 'about',
                'contact' => 'contact',
            );
            
            $this->data['uri_fr'] = 'fr/' . $uri;
            if(array_key_exists($uri, $uri_to_en)) {
                $this->data['uri_en'] = 'en/' . $uri_to_en[$uri];
            }
            else {
                $this->data['uri_en'] = 'fr/' . $uri;
            }
            
        }




        return $template->render($this->data);
    }

    private function getCurrentTemplateName() {

        $uri_segments = explode('/', $this->uri_string);

//        $controllerName = 'page/';
//        $actionName = 'index';
//        
        $i = 0;
//        
        foreach ($uri_segments as $u) {
            if ($u == '') {
                $i++;
            } else {
                break;
            }
        }

//        
//        if(isset($uri_segments[$i++])) {
//            $actionName = $uri_segments[$i++];
//            $actionName = str_replace('-', '_', $actionName);
//        }

        return 'page/' . $uri_segments[$i];
    }

    private function registerTwigFunctions() {
        $js_function = new Twig_SimpleFunction('js', function ($js) {
            echo base_url(JS_PATH . $js . '.js');
        });
        $images_function = new Twig_SimpleFunction('img', function ($img) {
            echo base_url(IMAGES_PATH . $img);
        });
        $css_function = new Twig_SimpleFunction('css', function ($css) {
            echo base_url(CSS_PATH . $css . '.css');
        });
        $link_function = new Twig_SimpleFunction('link', function ($uri) {
            echo base_url($uri);
        });
        $trans_function = new Twig_SimpleFunction('trans', function ($line, $lang_ = '') {
            global $LANG;

            return ($t = $LANG->line($line)) ? $t : $line;
        });

        $this->twig_instance->addFunction($js_function);
        $this->twig_instance->addFunction($images_function);
        $this->twig_instance->addFunction($css_function);
        $this->twig_instance->addFunction($link_function);

        $this->twig_instance->addFunction($trans_function);
    }

    private function registerTwigFilters() {
        $filterTranslate = new Twig_SimpleFilter('t', function ($line) {
            global $LANG;

            return ($t = $LANG->line($line)) ? $t : $line;
        });

        $this->twig_instance->addFilter($filterTranslate);
    }

    protected function loadLang($lang) {
        $this->lang->load($lang, $this->config->item('language'));
    }

}

?>
