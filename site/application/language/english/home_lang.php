<?php

$lang['home_title'] = "thinking ahead";
$lang['home_subtitle'] = "Revealing the potential that was overlooked by many is how you create opportunities.";
$lang['home_text'] = "MEZZANINE REIM is an investment manager specialized in real estate strategies. We execute core plus, value-added and opportunistic investment strategies that are designed to enhance overall value for investors while minimizing risk. We operate in Europe and the US within several asset types: residential, retail and office buildings. To achieve excellence, we capitalize on our market intelligence and combine diligence and creativity in our investment philosophy.";
$lang['home_more'] = "learn more";

$lang['home_map_title'] = "Targeted markets";
$lang['home_map_text'] = "Strategic asset allocation led us to diversify our sources of value creation both vertically on various asset profiles (residential, retail and offices) and horizontally, across geographical regions.";
$lang['home_map_primary'] = "Primary";
$lang['home_map_secondary'] = "Secondary";

$lang['home_perf_subtitle'] = "annualized return unleveraged";
$lang['home_perf_text'] = "All our decisions are subject to a process of evaluation and arbitration which aims to meet our investors' expectations.<br>Overall, this approach has enabled us to exceed an annualize return of 16% unlevered.";
