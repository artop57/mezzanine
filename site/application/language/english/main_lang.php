<?php

// NAV

$lang['nav_home_link'] = 'en/home';
$lang['nav_investment_link'] = 'en/investment';
$lang['nav_management_link'] = 'en/advisory';
$lang['nav_about_link'] = 'en/about';
$lang['nav_contact_link'] = 'en/contact';

$lang['nav_home'] = 'HOME';
$lang['nav_investment'] = 'INVESTMENT';
$lang['nav_management'] = 'ADVISORY';
$lang['nav_about'] = 'ABOUT';
$lang['nav_contact'] = 'CONTACT';