<?php

$lang['about_page_title'] = 'About';

// PICTO

$lang['about_picto_1_title'] = "strategic thinking";
$lang['about_picto_1_text'] = "We know where we want to go, visualize our goal, and lay down a strategic business plan before we commit to an opportunity.";

$lang['about_picto_2_title'] = "passion";
$lang['about_picto_2_text'] = "Our desire to shape assets and create value runs deep, which is why we handle every project with fresh thinking and enthusiasm.";

$lang['about_picto_3_title'] = "engagement";
$lang['about_picto_3_text'] = "We engage into business with a partner's mindset rather than detached client servicing. Our interest are fully aligned with those of our investors.";

// PARTNERS

$lang['about_team'] = "team";
$lang['about_partner'] = "Partner";

$lang['about_partner_1_desc_1'] = "Before launching MEZZANINE REIM, Yoann created a private investment company focused on value add and opportunistic strategies in the Paris area.";
$lang['about_partner_1_desc_2'] = "Prior to that, Yoann managed €1Bn in commercial assets at HAMMERSON, including office buildings and shopping centres, and realized €700M in arbitrage operations. ";
$lang['about_partner_1_desc_3'] = "Yoann also worked in real estate financing for institutional clients at HSBC REAL ESTATE.";

$lang['about_partner_2_desc_1'] = "Prior to MEZZANINE REIM, Mark launched CAPE MANAGEMENT GLOBAL in 2008, an investment trust looking to capture opportunities arising the price correction in the US real estate market.";
$lang['about_partner_2_desc_2'] = "CAPE MANAGEMENT GLOBAL managed 60 properties within 7 SPVs.";
$lang['about_partner_2_desc_3'] = "Before launching CMG, Mark managed over €1Bn of derivative assets successively at UBS, NOMURA and LEHMAN BROTHERS.";