<?php

// FORM

$lang['contact_form_text'] = 'We want to hear about your projects. Contact us.';

$lang['contact_form_error'] = 'An error has occured. Please try again.';
$lang['contact_form_error_validation'] = 'There are errors in the form. Please correct them.';
$lang['contact_form_sent'] = 'Your message has been sent !';

$lang['contact_form_name_placeholder'] = 'Your name';
$lang['contact_form_email_placeholder'] = 'Your email';
$lang['contact_form_phone_placeholder'] = 'Your phone (optional)';
$lang['contact_form_message_placeholder'] = 'Your message';
$lang['contact_form_submit'] = 'Send';