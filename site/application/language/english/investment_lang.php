<?php

$lang['investment_page_title'] = 'Investment';

// PICTOS

$lang['investment_picto_1_subtitle'] = "€ bn of asset priced";
$lang['investment_picto_1_text'] = "Each investment opportunity is analysed extensively: market dynamics, environment, valuation of the asset and its potential.";

$lang['investment_picto_2_subtitle'] = "annualized return unleveraged";
$lang['investment_picto_2_text'] = "All our decisions are subject to a process of evaluation and arbitration which aims to meet our investors’ expectations.";

$lang['investment_picto_3_subtitle'] = "units";
$lang['investment_picto_3_text'] = "To achieve a successful execution of the strategy, we keep full control over Asset Management both at the operational and administrative level.";

// ROW 1

$lang['investment_row_1_title_1'] = "how we do";
$lang['investment_row_1_text_1'] = "Through our top-down analysis we are able to identify trends and target opportunities in markets that are at different points in the cycle. By integrating this information into a bottom-up approach we improve our asset management decisions.";

$lang['investment_row_1_title_2'] = "what we offer";
$lang['investment_row_1_text_2'] = "We tailor bespoke investments strategies to help our clients achieve their objectives. Our independent team actively manages their portfolio each step of the way. In this transparent process our interests are fully aligned with those of our investors. We target acquisitions ranging from €1m to €15m. The rental profile varies depending on the market and the asset type.";

// ROW 2

$lang['investment_row_2_title_1'] = "acquisition";
$lang['investment_row_2_text_1'] = "MEZZANINE’s expertise in residential and commercial markets allows us to identify relevant investment opportunities ahead of the curve. A successful acquisition means buying at the fair price, at the right time. Each step of the process is duly handled to make sure a deal is realized on the best terms.";

$lang['investment_row_2_accordeon_1_title'] = "market researhc & origination";
$lang['investment_row_2_accordeon_1_text_1'] = "Analyse the economic environment and its implications on real estate to idenAfy areas with potential";
$lang['investment_row_2_accordeon_1_text_2'] = "Analyse supply/demand dynamics, and trends by region and by asset types on these targeted areas";
$lang['investment_row_2_accordeon_1_text_3'] = "Monitor markets to identify relevant investment opportunities";
$lang['investment_row_2_accordeon_1_text_4'] = "Isolate an asset with strong potential";

$lang['investment_row_2_accordeon_2_title'] = "strategic planning & pricing";
$lang['investment_row_2_accordeon_2_text_1'] = "Analyse asset’ strengths and opportunities";
$lang['investment_row_2_accordeon_2_text_2'] = "Identify creative ways to increase the asset value with the Asset Managers";
$lang['investment_row_2_accordeon_2_text_3'] = "Carry out the financial modeling of each relevant scenario and estimate the total cost of the project";
$lang['investment_row_2_accordeon_2_text_4'] = "Determine the purchase price using the selected assumptions";

$lang['investment_row_2_accordeon_3_title'] = "execution";
$lang['investment_row_2_accordeon_3_text_1'] = "Coordinate the due diligence process";
$lang['investment_row_2_accordeon_3_text_2'] = "Supervise the drafting of legal statements";
$lang['investment_row_2_accordeon_3_text_3'] = "Realize the transaction’s closing and search for financing";

// ROW 3

$lang['investment_row_3_title_1'] = "investment management";
$lang['investment_row_3_text_1'] = "Our approach aims at creating value continuously throughout the investment cycle, by implementing analytical and diligent processes of checks and balances. To achieve our investors’ objectives, we apply our expertise wherever we see an opportunity to add value to an asset, while controlling risks at the same time.";

$lang['investment_row_3_accordeon_1_title'] = "asset management";
$lang['investment_row_3_accordeon_1_text_1'] = "Identify the key drivers of value creation: opportunities for reconfiguration or development based on planning regulations, analysis of the leasing strategy etc.";
$lang['investment_row_3_accordeon_1_text_2'] = "Implement a strategic plan to increase the value of the asset, and prepare an annual budget";
$lang['investment_row_3_accordeon_1_text_3'] = "Coordinate the project management and implement the strategic plan using a team with complementary skills, : architects, building contractors, lawyers, and tax specialists";
$lang['investment_row_3_accordeon_1_text_4'] = "Control and manage treasury issues at the asset level and prepare the financial reports";

$lang['investment_row_3_accordeon_2_title'] = "portfolio management & investors relations";
$lang['investment_row_3_accordeon_2_text_1'] = "Establish the optimal holding period and prepare an exit strategy";
$lang['investment_row_3_accordeon_2_text_2'] = "Manage funding at the portfolio level as well as (re)financing issues";
$lang['investment_row_3_accordeon_2_text_3'] = "Inform investors about the main events that occur in their portfolio";
$lang['investment_row_3_accordeon_2_text_4'] = "Send the funds’ management and performance reports to stakeholders (lenders or investors)";

// ROW 4

$lang['investment_row_4_title_1'] = "property management";
$lang['investment_row_4_text_1'] = "The purpose of our Property Management service is to optimize the way the asset is handled operationally, whether we are talking about rents, technical specifications of a building, accounting, or other administrative issues. In an effort to anticipate problems we implement preventive solutions and maintain excellent communication with all parties.";
$lang['investment_row_4_text_2'] = "Manage the top line and control operating expenses to improve the asset’s profitability, and value";
$lang['investment_row_4_text_3'] = "Orchestrate the relationship with tenants";
$lang['investment_row_4_text_4'] = "Comply with the technical requirements of a building";
$lang['investment_row_4_text_5'] = "Develop a management policy in line with regulatory changes and environmental issues";
$lang['investment_row_4_text_6'] = "Represent the landlord in front of tenants, owners’ association, and the administration";
$lang['investment_row_4_text_7'] = "Manage cash flows and handle the reporting by asset and by tenant";
$lang['investment_row_4_text_8'] = "Manage litigation";