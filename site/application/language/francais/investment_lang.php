<?php

$lang['investment_page_title'] = 'Investissement';

// PICTOS

$lang['investment_picto_1_subtitle'] = "mds € d'actifs étudiés";
$lang['investment_picto_1_text'] = "Chaque opportunité d’investissement est analysée de façon détaillée : dynamiques de marché, environnement, valorisation de l’actif et de son potentiel.";

$lang['investment_picto_2_subtitle'] = "rendement annualié hors financement";
$lang['investment_picto_2_text'] = "Toutes nos décisions font l’objet d’un processus d’évaluation et d’arbitrage qui a pour finalité de répondre aux attentes des investisseurs.";

$lang['investment_picto_3_subtitle'] = "unités";
$lang['investment_picto_3_text'] = "Pour assurer la bonne exécution de la stratégie, nous gardons la maîtrise de l’Asset Management au niveau opérationnel et administratif.";

// ROW 1

$lang['investment_row_1_title_1'] = "notre approche";
$lang['investment_row_1_text_1'] = "Une analyse top-down nous permet d’identifier des tendances et de cibler des opportunités sur des marchés qui se situent à des moments différents du cycle immobilier tandis qu’une analyse bottom-up est utilisée pour intégrer les informations de marché aux décisions d’asset management.";

$lang['investment_row_1_title_2'] = "notre offre";
$lang['investment_row_1_text_2'] = "Nous definissions des stratégies sur-mesure pour et avec nos clients, en tenant compte de leurs objectifs. Notre plateforme permet l'investissement via des fonds fermes ou comptes separes. Les investissements sont geres activement et de facon transparente par une équipe independante dont les interets sont 100% alignés avec ceux des clients a toutes les étapes du projet.";

// ROW 2

$lang['investment_row_2_title_1'] = "acquisition";
$lang['investment_row_2_text_1'] = "L’expertise de MEZZANINE sur les marchés résidentiels, commerces et bureaux permet d’identifier en amont des opportunités d’investissement pertinentes. Chacune des étapes de l’investissement est attentivement étudiée afin de réaliser l’opération à la juste valeur et au bon moment.";

$lang['investment_row_2_accordeon_1_title'] = "analyse du marché";
$lang['investment_row_2_accordeon_1_text_1'] = "Analyser la conjoncture économique et ses effets sur l’immobilier pour identifier des marchés à potentiel";
$lang['investment_row_2_accordeon_1_text_2'] = "Analyser les dynamiques d’offre et de demande, les tendances par régions et par typologie d’actifs sur les marchés ciblés";
$lang['investment_row_2_accordeon_1_text_3'] = "Mettre en place une veille de marché étendue et identifier des opportunités d’investissement";
$lang['investment_row_2_accordeon_1_text_4'] = "Isoler un actif à potentiel";

$lang['investment_row_2_accordeon_2_title'] = "définition d'une stratégie et pricing";
$lang['investment_row_2_accordeon_2_text_1'] = "Analyser les forces et faiblesses de l’actif dans son environnement";
$lang['investment_row_2_accordeon_2_text_2'] = "Identifier les leviers de création de valeur en lien avec l’Asset Management";
$lang['investment_row_2_accordeon_2_text_3'] = "Modéliser les scénarios étudiés et définir les besoins financiers de l’opération";
$lang['investment_row_2_accordeon_2_text_4'] = "Fixer un prix d’acquisition en fonction des hypothèses retenues";

$lang['investment_row_2_accordeon_3_title'] = "exécution";
$lang['investment_row_2_accordeon_3_text_1'] = "Coordonner la phase de due diligence";
$lang['investment_row_2_accordeon_3_text_2'] = "Superviser la rédaction des actes juridiques";
$lang['investment_row_2_accordeon_3_text_3'] = "Réaliser la transaction et accompagner dans la recherche de financements";

// ROW 3

$lang['investment_row_3_title_1'] = "investment management";
$lang['investment_row_3_text_1'] = "Notre approche vise à créer de la valeur de façon continue tout au long du cycle d’investissement et à répondre ainsi aux objectifs de nos investisseurs tout en maîtrisant les risques. Nous exerçons notre expertise sur l’ensemble des aspects relatifs à l’actif et à sa valorisation à travers un processus analytique et méthodique.";

$lang['investment_row_3_accordeon_1_title'] = "asset management";
$lang['investment_row_3_accordeon_1_text_1'] = "Identifier les leviers de création de valeur : possibilités de reconfiguration ou de développement en fonction des règlements d’urbanisme, analyse de la stratégie locative etc.";
$lang['investment_row_3_accordeon_1_text_2'] = "Implémenter un plan de valorisation stratégique ainsi qu'un budget opérationnel annuel";
$lang['investment_row_3_accordeon_1_text_3'] = "Coordonner la gestion des projets liés à l’exécution de la stratégie autour d’une équipe pluridisciplinaire : architectes, avocats, fiscalistes et experts immobiliers";
$lang['investment_row_3_accordeon_1_text_4'] = "Analyser continuellement les problématiques de trésorerie au niveau de l’actif et préparer les états";

$lang['investment_row_3_accordeon_2_title'] = "portfolio management et relation investisseurs";
$lang['investment_row_3_accordeon_2_text_1'] = "Déterminer la période de détention op;male et la stratégie de vente";
$lang['investment_row_3_accordeon_2_text_2'] = "Gérer la trésorerie au niveau du fonds et les caractéristiques des financements ou refinancements nécessaires";
$lang['investment_row_3_accordeon_2_text_3'] = "Informer les investisseurs des principaux évènements survenus dans leur portefeuille";
$lang['investment_row_3_accordeon_2_text_4'] = "Transmettre les rapports de gestion et de performance du fonds aux prêteurs et investisseurs";

// ROW 4

$lang['investment_row_4_title_1'] = "property management";
$lang['investment_row_4_text_1'] = "Les services Property Management optimisent la gestion locative, technique, financière et administrative du patrimoine de nos investisseurs. Nous nous efforçons d’anticiper les problèmes par la mise en place de solutions préventives et à entretenir une bonne communication dans un climat de confiance avec l’ensemble des intervenants.";
$lang['investment_row_4_text_2'] = "Optimisant les revenus et maîtriser les charges liées à l’exploitation pour accroître la valeur des actifs";
$lang['investment_row_4_text_3'] = "Orchestrer la relation avec les locataires";
$lang['investment_row_4_text_4'] = "Coordonner la gestion technique des immeubles";
$lang['investment_row_4_text_5'] = "Développer une politique de gestion en accord avec les évolutions règlementaires et enjeux environnamentaux";
$lang['investment_row_4_text_6'] = "Représenter le propriétaire auprès des locataires, syndic, organismes sociaux et administration fiscale";
$lang['investment_row_4_text_7'] = "Gérer les flux financiers et établir un reporting par actif et par locataire";
$lang['investment_row_4_text_8'] = "Gérer les procédures contentieuses";