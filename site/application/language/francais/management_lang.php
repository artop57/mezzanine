<?php

// NAV

$lang['nav_home_link'] = 'fr/accueil';
$lang['nav_investment_link'] = 'fr/investissement';
$lang['nav_management_link'] = 'fr/conseil';
$lang['nav_about_link'] = 'fr/a-propos';
$lang['nav_contact_link'] = 'fr/contact';