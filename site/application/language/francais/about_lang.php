<?php

$lang['about_page_title'] = 'A propos';

// PICTO

$lang['about_picto_1_title'] = "vision stratégique";
$lang['about_picto_1_text'] = "Nous savons où nous voulons aller. Avant de nous engager dans une direction, nous mettons en place un business plan stratégique pour garder le cap.";

$lang['about_picto_2_title'] = "passion";
$lang['about_picto_2_text'] = "Notre engouement à façonner et apporter de la valeur aux actifs est profond. C’est ce qui nous permet d’aborder chaque projet avec enthousiasme et avec un regard nouveau.";

$lang['about_picto_3_title'] = "engagement";
$lang['about_picto_3_text'] = "Nous appréhendons nos relations avec l’état d’esprit d’un partenaire. Nos intérêts sont totalement alignés sur ceux de nos investisseurs.";

// PARTNERS

$lang['about_team'] = "l'équipe";
$lang['about_partner'] = "Associé";

$lang['about_partner_1_desc_1'] = "Avant de fonder MEZZANINE REIM, Yoann a crée une société d’investissement privée axée sur des stratégies value added et opportunistes à Paris.";
$lang['about_partner_1_desc_2'] = "Auparavant, Yoann a géré un milliard d’euros d’actifs, immeubles de bureaux et centres commerciaux, au sein de HAMMERSON et a réalisé 700 millions d’euros d’opérations d’arbitrage.";
$lang['about_partner_1_desc_3'] = "Il a également travaillé en financements structurés immobilier pour clients institutionnels chez HSBC REAL ESTATE.";

$lang['about_partner_2_desc_1'] = "Avant le lancement de MEZZANINE REIM, Mark a fondé CAPE MANAGEMENT GLOBAL en 2008, un fonds privé visant à capter les opportunités résultant de l'ajustement des prix sur le marché immobilier américain.";
$lang['about_partner_2_desc_2'] = "A ce jour, CAPE MANAGEMENT GLOBAL gère 60 propriétés détenues au sein de 7 véhicules d’investissement.";
$lang['about_partner_2_desc_3'] = "Auparavant, Mark a géré plus d’un milliard d’euros d’actifs dérivés successivement auprès d‘UBS, NOMURA et LEHMAN BROTHERS.";