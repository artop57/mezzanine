<?php

$lang['home_title'] = "un coup d'avance";
$lang['home_subtitle'] = "C'est en révélant le potentiel insoupçonné que l'on se crée des opportunités.";
$lang['home_text'] = "MEZZANINE REIM est une plateforme d'investissement immobilier sur des stratégies core plus, value added et opportunistes. Nous opérons en Europe et aux Etats-Unis sur les secteurs résidentiels, commerces et bureaux. Nous capitalisons sur notre connaissance de ces marchés et sur une approche à la fois méthodique et créative.";
$lang['home_more'] = "en savoir plus";

$lang['home_map_title'] = "Marchés cibles";
$lang['home_map_text'] = "Une allocation stratégique de nos actifs nous conduit à diversifier les sources de création de valeur à la fois verticalement, sur des profils d'actifs variés (résidentiels, commerces et bureaux) et horizontalement, sur différentes régions géographiques.";
$lang['home_map_primary'] = "Primaires";
$lang['home_map_secondary'] = "Secondaires";

$lang['home_perf_subtitle'] = "rendement annualisé hors financement";
$lang['home_perf_text'] = "Toutes nos décisions font l'objet d'un processus d'évaluation et d'arbitrage qui a pour finalité de répondre aux attentes des investisseurs.<br>Cette approche nous a permis d'atteindre, sur l'ensemble de nos opérations, un rendement annualisé supérieur à 16% hors financement.";
