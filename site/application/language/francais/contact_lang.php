<?php

// FORM

$lang['contact_form_text'] = 'Vos projets nous intéressent. Contactez-nous.';

$lang['contact_form_error'] = 'Une erreur est survenue. Veuillez réessayer';
$lang['contact_form_error_validation'] = 'Il y a des erreurs dans le formulaire. Veuillez les corriger.';
$lang['contact_form_sent'] = 'Votre message a été envoyé !';

$lang['contact_form_name_placeholder'] = 'Votre nom';
$lang['contact_form_email_placeholder'] = 'Votre email';
$lang['contact_form_phone_placeholder'] = 'Votre téléphone (facultatif)';
$lang['contact_form_message_placeholder'] = 'Votre message';
$lang['contact_form_submit'] = 'Envoyer';